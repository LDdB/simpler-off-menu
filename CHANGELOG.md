# CHANGELOG
### v. 6  
##### Released on 2020-6-6  

  - Fixed a Log Out and Power Off names to show it on the system language 
  - Added a option to Suspend in the Menu.
  - Added a option to Hibernate in the Menu.
  - Updated the expansion settings to allow the user set their preference on Gnome Tweaks options.

### v. 5  
##### Released on 2020-5-30  

  - Fixed a bug where changing options bring a error 
  - Added a option to Logout from the Menu.
  - Added a option to merge Logout and PowerOff in just one button, launching with left-click and right-click.
  - Updated the expansion settings to allow the user set their preference on Gnome Tweaks options.

### v. 4  
##### Released on 2020-5-22  

  - Allow to toggle Settings and Lock independently.
  - Added a option to launch Gnome-tweaks from the Menu.
  - Added a option to merge Settings and Gnome-tweaks in just one button, launching with left-click and right-click.
  - Updated the expansion settings to allow the user set their preference on Gnome Tweaks options.

### v. 3  
##### Released on 2020-5-21  

  - Restructuring and cleaning of the code, use strict mode and Lang deprecated in favour of ES6 syntax.
  - This version does not add any improvements for users, so it will not be sent to gnome.extensions

### v. 2  
##### Realeased on 2020-5-13

 - Added a option to keep showing the Settings and Block options in the menu 
 
### v. 1  
##### Realeased on 2020-5-5
  
 - First version launched 
___

