'use strict';

const { GObject, Shell, Gio, St } = imports.gi;
const Main = imports.ui.main;
const Menu = Main.panel.statusArea.aggregateMenu.menu;
const PopupMenu = imports.ui.popupMenu;
const SystemActions = imports.misc.systemActions;
const Me = imports.misc.extensionUtils.getCurrentExtension();
const Util = imports.misc.util;

let customMenu;
let separator1, separator2, lock, poweroff, settingsicon, gnomeTweaks, logout, suspend, hibernate, reboot;


let showsettings, showlock, showgnomeTweaks, showlogout, showhibernate, showsuspend;
let mergesettingstweaks, mergelogoutpoweroff;

let settingsApp = Shell.AppSystem.get_default().lookup_app('gnome-control-center.desktop');
let gnomeTweaksApp = Shell.AppSystem.get_default().lookup_app('org.gnome.tweaks.desktop');

function getSettings(){
let GioSSS = Gio.SettingsSchemaSource;
let schemaSource = GioSSS.new_from_directory( Me.dir.get_child("schemas").get_path(), GioSSS.get_default(), false );
let schemaObj = schemaSource.lookup('org.gnome.shell.extensions.simpleroffmenu', true);
if(!schemaObj){
throw new Error ('cannot find schemas');
}
return new Gio.Settings({ settings_schema : schemaObj });
}

function MergeSettingtweak(widget, event) {
	if (event.get_button() == 1) {
	Main.overview.hide();
	settingsApp.activate();
	}
	else if (event.get_button() == 3) {
	Main.overview.hide();
	gnomeTweaksApp.activate();
	}
}

function MergeLogoutPoweroff(widget, event) {
	if (event.get_button() == 1) {
	Main.overview.hide();
	    try {
        Util.trySpawnCommandLine('gnome-session-quit --power-off');
    } catch (err) {
        Main.notify("Error " + err);
    }
	}
	else if (event.get_button() == 3) {
	Main.overview.hide();	    try {
        Util.trySpawnCommandLine('gnome-session-quit --logout');
    } catch (err) {
        Main.notify("Error " + err);
    }
	}
}

function FuncHibernate(widget, event) {
	Main.overview.hide();	    try {
       Util.trySpawnCommandLine('systemctl hibernate');
    } catch (err) {
        Main.notify("Error " + err);
    }
}

function FuncReboot(widget, event) {
	Main.overview.hide();	    try {
       Util.trySpawnCommandLine('gnome-session-quit --reboot --force');
    } catch (err) {
        Main.notify("Error " + err);
    }
}


class CustomMenu {
    constructor() {
        this._system = Main.panel.statusArea.aggregateMenu._system;
        this._systemActions = new SystemActions.getDefault();
        this.poblarMenu();
    
        Menu.box.remove_actor(this._system.menu.actor);
    }

    poblarMenu() {
        let bindFlags = GObject.BindingFlags.DEFAULT;
        
        separator1 = new PopupMenu.PopupSeparatorMenuItem;
        Menu.addMenuItem(separator1);

        
        
        if(mergesettingstweaks){
            if(showsettings){
                if (settingsApp) {
                    let [name, icon] = [ settingsApp.get_name(), settingsApp.app_info.get_icon().names[0] ];
                    settingsicon = new PopupMenu.PopupImageMenuItem(name, icon);
                    settingsicon.connect('button-press-event', MergeSettingtweak);
                    Menu.addMenuItem(settingsicon); }
                else { log('Missing Settings'); settings = new St.Widget(); }
            }
        }else{
            if(showsettings){
                if (settingsApp) {
                    let [name, icon] = [ settingsApp.get_name(), settingsApp.app_info.get_icon().names[0] ];
                    settingsicon = new PopupMenu.PopupImageMenuItem(name, icon);
                    settingsicon.connect('activate', () => { Main.overview.hide(); settingsApp.activate(); });
                    Menu.addMenuItem(settingsicon); }
                else { log('Missing Settings'); settings = new St.Widget(); }
            }
            if(showgnomeTweaks){
                if (gnomeTweaksApp) {
                    let [name, icon] = [ gnomeTweaksApp.get_name(), gnomeTweaksApp.app_info.get_icon().names[0], ];
                    gnomeTweaks = new PopupMenu.PopupImageMenuItem(name, icon);
                    gnomeTweaks.connect('activate', () => { Main.overview.hide(); gnomeTweaksApp.activate(); }); 
                    Menu.addMenuItem(gnomeTweaks); }
                else { log('Missing gnomeTweaks'); gnomeTweaks = new St.Widget(); }
            }
        }
        

        if(showlock){
            lock = new PopupMenu.PopupImageMenuItem(_('Lock'), 'changes-prevent-symbolic');
            lock.connect('activate', () => { this._systemActions.activateLockScreen(); });
            Menu.addMenuItem(lock);
            this._systemActions.bind_property('can-lock-screen', lock, 'visible', bindFlags);
        }

        separator2 = new PopupMenu.PopupSeparatorMenuItem;
        Menu.addMenuItem(separator2);

        if(showsuspend){
            suspend = new PopupMenu.PopupImageMenuItem(_('Suspend'), 'changes-prevent-symbolic');
            suspend.connect('activate', () => { this._systemActions.activateSuspend(); });
            Menu.addMenuItem(suspend);
            this._systemActions.bind_property('can-suspend', suspend, 'visible', bindFlags);
        }

        if(showhibernate){
            hibernate = new PopupMenu.PopupImageMenuItem(_('Hibernate'), 'changes-prevent-symbolic');
            hibernate.connect('button-press-event', FuncHibernate);
            Menu.addMenuItem(hibernate);
        }
      
       if(mergelogoutpoweroff){  
       
        reboot = new PopupMenu.PopupImageMenuItem(_('Restart…'), 'system-reboot-symbolic');
        reboot.connect('button-press-event', FuncReboot);
        Menu.addMenuItem(reboot);

        poweroff = new PopupMenu.PopupImageMenuItem(_('Power Off…'), 'system-shutdown-symbolic');
        poweroff.connect('button-press-event', MergeLogoutPoweroff);
        Menu.addMenuItem(poweroff);
        
       }else {
            if(showlogout){
            logout = new PopupMenu.PopupImageMenuItem(_('Log Out'), 'system-log-out-symbolic');
            logout.connect('activate', () => { this._systemActions.activateLogout(); });
            Menu.addMenuItem(logout);
            this._systemActions.bind_property('can-logout', logout, 'visible', bindFlags);
            }
       
            reboot = new PopupMenu.PopupImageMenuItem(_('Restart…'), 'system-reboot-symbolic');
            reboot.connect('button-press-event', FuncReboot);
            Menu.addMenuItem(reboot);

            poweroff = new PopupMenu.PopupImageMenuItem(_('Power Off…'), 'system-shutdown-symbolic');
            poweroff.connect('activate', () => { this._systemActions.activatePowerOff(); });
            Menu.addMenuItem(poweroff);
            this._systemActions.bind_property('can-power-off', poweroff, 'visible', bindFlags);
       }
       
    }
    
    

    destroy() {
        Menu.box.remove_actor(separator1);
        Menu.box.remove_actor(poweroff);
        if(showsettings){
            Menu.box.remove_actor(settingsicon);
        }
        if(showgnomeTweaks && !mergesettingstweaks){
            Menu.box.remove_actor(gnomeTweaks);
        }
        if(showlock){
            Menu.box.remove_actor(lock);
        }
        if(showsuspend){
            Menu.box.remove_actor(suspend);
        }
        if(showhibernate){
            Menu.box.remove_actor(hibernate);
        }
        Menu.box.remove_actor(reboot);
        if(showlogout && !mergelogoutpoweroff){
            Menu.box.remove_actor(logout);
        }
        Menu.box.remove_actor(separator2);
        Menu.box.add_actor(this._system.menu.actor);
    }
}


function init() {
}

function enable() {
let settings = getSettings();
showsettings = settings.get_boolean('show-settings');
showlock = settings.get_boolean('show-lock');
showgnomeTweaks = settings.get_boolean('show-tweak');
mergesettingstweaks = settings.get_boolean('merge-settings-tweak');
showlogout = settings.get_boolean('show-logout');
mergelogoutpoweroff = settings.get_boolean('merge-logout-poweroff');
showsuspend = settings.get_boolean('show-suspend');
showhibernate = settings.get_boolean('show-hibernate');

customMenu = new CustomMenu();
}

function disable() {
customMenu.destroy();
}

