'use strict';

const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();


function init() {
    log(`initializing ${Me.metadata.name} Preferences`);
}

function buildPrefsWidget() {

    // Copy the same GSettings code from `extension.js`
    let gschema = Gio.SettingsSchemaSource.new_from_directory(
        Me.dir.get_child('schemas').get_path(),
        Gio.SettingsSchemaSource.get_default(),
        false
    );
    
    this.settings = new Gio.Settings({
        settings_schema: gschema.lookup('org.gnome.shell.extensions.simpleroffmenu', true)
    });

    // Create a parent widget that we'll return from this function
    let prefsWidget = new Gtk.Grid({
        margin: 18,
        column_spacing: 12,
        row_spacing: 12,
        visible: true
    });

    ////
    // Create a label & switch for `show-settings`
    let toggleLabel1 = new Gtk.Label({
        label: 'Show Setting in menu:',
        halign: Gtk.Align.START,
        visible: true
    });
    prefsWidget.attach(toggleLabel1, 0, 1, 1, 1);

    let toggle1 = new Gtk.Switch({
        active: this.settings.get_boolean ('show-settings'),
        halign: Gtk.Align.END,
        visible: true
    });
    prefsWidget.attach(toggle1, 1, 1, 1, 1);
    
    // Bind the switch to the `show-settings` key
    this.settings.bind(
        'show-settings',
        toggle1,
        'active',
        Gio.SettingsBindFlags.DEFAULT
    );
    
    
    ////
    // Create a label & switch for `show-lock`
    let toggleLabel2 = new Gtk.Label({
        label: 'Show Lock in menu:',
        halign: Gtk.Align.START,
        visible: true
    });
    prefsWidget.attach(toggleLabel2, 0, 2, 1, 1);

    let toggle2 = new Gtk.Switch({
        active: this.settings.get_boolean ('show-lock'),
        halign: Gtk.Align.END,
        visible: true
    });
    prefsWidget.attach(toggle2, 1, 2, 1, 1);
    
    // Bind the switch to the `show-lock` key
    this.settings.bind(
        'show-lock',
        toggle2,
        'active',
        Gio.SettingsBindFlags.DEFAULT
    );
    
    
    ////
    // Create a label & switch for `show-tweak`
    let toggleLabel3 = new Gtk.Label({
        label: 'Show Gnome-Tweak in menu *:',
        halign: Gtk.Align.START,
        visible: true
    });
    prefsWidget.attach(toggleLabel3, 0, 3, 1, 1);

    let toggle3 = new Gtk.Switch({
        active: this.settings.get_boolean ('show-tweak'),
        halign: Gtk.Align.END,
        visible: true
    });
    prefsWidget.attach(toggle3, 1, 3, 1, 1);
    
    let toggleLabel3_2 = new Gtk.Label({
        label: '* If you choose to merge Settings and Gnome-Tweak, this toggle is ignored',
        halign: Gtk.Align.START,
        visible: true
    });
    prefsWidget.attach(toggleLabel3_2, 2, 3, 1, 1);
    
    // Bind the switch to the `show-tweak` key
    this.settings.bind(
        'show-tweak',
        toggle3,
        'active',
        Gio.SettingsBindFlags.DEFAULT
    );
    
    
    ////
    // Create a label & switch for `merge-settings-tweak`
    let toggleLabel4 = new Gtk.Label({
        label: 'Merge Settings and Gnome-Tweak in menu:',
        halign: Gtk.Align.START,
        visible: true
    });
    prefsWidget.attach(toggleLabel4, 0, 4, 1, 1);

    let toggle4 = new Gtk.Switch({
        active: this.settings.get_boolean ('merge-settings-tweak'),
        halign: Gtk.Align.END,
        visible: true
    });
    prefsWidget.attach(toggle4, 1, 4, 1, 1);
    
    let toggleLabel4_2 = new Gtk.Label({
        label: '(left-click to launch Settings, right-click to launch Gnome-Tweaks)',
        halign: Gtk.Align.START,
        visible: true
    });
    prefsWidget.attach(toggleLabel4_2, 2, 4, 1, 1);
    
    // Bind the switch to the `merge-settings-tweak` key
    this.settings.bind(
        'merge-settings-tweak',
        toggle4,
        'active',
        Gio.SettingsBindFlags.DEFAULT
    );


    ////
    // Create a label & switch for `show-logout`
    let toggleLabel5 = new Gtk.Label({
        label: 'Show Logout in menu **:',
        halign: Gtk.Align.START,
        visible: true
    });
    prefsWidget.attach(toggleLabel5, 0, 5, 1, 1);

    let toggle5 = new Gtk.Switch({
        active: this.settings.get_boolean ('show-logout'),
        halign: Gtk.Align.END,
        visible: true
    });
    prefsWidget.attach(toggle5, 1, 5, 1, 1);
    
    let toggleLabel5_2 = new Gtk.Label({
        label: '** If you choose to merge Logout and Poweroff, this toggle is ignored',
        halign: Gtk.Align.START,
        visible: true
    });
    prefsWidget.attach(toggleLabel5_2, 2, 5, 1, 1);
    
    // Bind the switch to the `show-logout` key
    this.settings.bind(
        'show-logout',
        toggle5,
        'active',
        Gio.SettingsBindFlags.DEFAULT
    );
    
    
    ////
    // Create a label & switch for `merge-logout-poweroff`
    let toggleLabel6 = new Gtk.Label({
        label: 'Merge Logout and Poweroff in menu:',
        halign: Gtk.Align.START,
        visible: true
    });
    prefsWidget.attach(toggleLabel6, 0, 6, 1, 1);

    let toggle6 = new Gtk.Switch({
        active: this.settings.get_boolean ('merge-logout-poweroff'),
        halign: Gtk.Align.END,
        visible: true
    });
    prefsWidget.attach(toggle6, 1, 6, 1, 1);
    
    let toggleLabel6_2 = new Gtk.Label({
        label: '(left-click to Power Off, right-click to Logout)',
        halign: Gtk.Align.START,
        visible: true
    });
    prefsWidget.attach(toggleLabel6_2, 2, 6, 1, 1);
    

    // Bind the switch to the `merge-logout-poweroff` key
    this.settings.bind(
        'merge-logout-poweroff',
        toggle6,
        'active',
        Gio.SettingsBindFlags.DEFAULT
    );


    ////
    // Create a label & switch for `show-suspend`
    let toggleLabel7 = new Gtk.Label({
        label: 'Show Suspend in menu:',
        halign: Gtk.Align.START,
        visible: true
    });
    prefsWidget.attach(toggleLabel7, 0, 7, 1, 1);

    let toggle7 = new Gtk.Switch({
        active: this.settings.get_boolean ('show-suspend'),
        halign: Gtk.Align.END,
        visible: true
    });
    prefsWidget.attach(toggle7, 1, 7, 1, 1);
    
    // Bind the switch to the `show-suspend` key
    this.settings.bind(
        'show-suspend',
        toggle7,
        'active',
        Gio.SettingsBindFlags.DEFAULT
    );


    ////
    // Create a label & switch for `show-hibernate`
    let toggleLabel8 = new Gtk.Label({
        label: 'Show Hibernate in menu: ***',
        halign: Gtk.Align.START,
        visible: true
    });
    prefsWidget.attach(toggleLabel8, 0, 8, 1, 1);

    let toggle8 = new Gtk.Switch({
        active: this.settings.get_boolean ('show-hibernate'),
        halign: Gtk.Align.END,
        visible: true
    });
    prefsWidget.attach(toggle8, 1, 8, 1, 1);
    
    let toggleLabel8_2 = new Gtk.Label({
        label: '*** The Hibernate function may not work on every system',
        halign: Gtk.Align.START,
        visible: true
    });
    prefsWidget.attach(toggleLabel8_2, 2, 8, 1, 1);
    
    // Bind the switch to the `show-hibernate` key
    this.settings.bind(
        'show-hibernate',
        toggle8,
        'active',
        Gio.SettingsBindFlags.DEFAULT
    );
    
    
    ////
    // Important Label
    let ImportantLabel1 = new Gtk.Label({
        label: '_____ ',
        halign: Gtk.Align.START,
        visible: true
    });
    prefsWidget.attach(ImportantLabel1, 1, 9, 3, 1);
    
    let ImportantLabel2 = new Gtk.Label({
        label: 'Important: After changing the settings, disable and re-enable the extension for the changes to take effect.',
        halign: Gtk.Align.START,
        visible: true
    });
    prefsWidget.attach(ImportantLabel2, 0, 10, 3, 1);


    // Return our widget which will be added to the window
    return prefsWidget;
}

